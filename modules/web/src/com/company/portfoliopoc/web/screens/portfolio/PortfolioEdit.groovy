package com.company.portfoliopoc.web.screens.portfolio

import com.company.portfoliopoc.service.PortfolioService
import com.company.portfoliopoc.service.SecurityService
import com.haulmont.charts.gui.amcharts.model.Color
import com.haulmont.charts.gui.amcharts.model.Guide;
import com.haulmont.charts.gui.components.charts.RadarChart;
import com.haulmont.charts.gui.data.ListDataProvider;
import com.haulmont.charts.gui.data.MapDataItem;
import com.haulmont.cuba.gui.components.AbstractEditor;
import com.company.portfoliopoc.entity.Portfolio;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.screen.Subscribe
import com.haulmont.cuba.web.gui.components.WebButton;

import javax.inject.Inject
import java.math.MathContext
import java.math.RoundingMode;

public class PortfolioEdit extends AbstractEditor<Portfolio> {
    private String ICON_NO_INTEREST = "font-icon:SQUARE_O";
    private String ICON_MINOR_INTEREST = "font-icon:MINUS_SQUARE_O";
    private String ICON_MAJOR_INTEREST = "font-icon:PLUS_SQUARE_O";

    @Inject
    private RadarChart polarChart;

    @Inject
    protected PortfolioService portfolioService

    @Inject
    protected SecurityService securityService

    @Subscribe
    protected void onInit(InitEvent event) {
        redrawChart()
    }

    int getScore(String category) {
        // For each proposed holding, find out it's score for each area
        // Work that out as percentage of theoretical max

        if (!getItem() || !getItem().proposedHoldings)
            return 0

        def totalScore = getItem().proposedHoldings.size() * 10
        def thisScore = 0

        getItem().proposedHoldings.each {
            thisScore += it.security.getValue(category)
        }

        return (thisScore / totalScore * 100).toInteger()
    }

    int getScoreRelToMax(String category) {
        // Determine the top value for this property
        // Determine the average of the value for all the holdings of this property
        // Work out one as pct of the otheer
        if (!getItem() || !getItem().proposedHoldings)
            return 0

        def avgScore = new BigDecimal(0)

        getItem().proposedHoldings.each {
            def value = it.security.getValue(category)
            if (value)
                avgScore = avgScore.add(it.security.getValue(category))
        }
        avgScore = avgScore.divide(new BigDecimal(getItem().proposedHoldings.size()),2, RoundingMode.HALF_EVEN)

        def maxSecurity = securityService.getSecurities().max{it.getValue(category)}
        def maxScore =  maxSecurity.getValue(category) as BigDecimal

        return (avgScore.divide(maxScore, 2, RoundingMode.HALF_EVEN).multiply(new BigDecimal(100), MathContext.DECIMAL64)).toInteger()
    }

    void redrawChart() {
        // Chart should contain two overlays:
        // 1. A representation of the options the user has selected (the 'ideal' selection)
        // 2. A representation of what has been achieved (the 'actual' selection)
        // This should help to draw attention when things start diverging
        // Also, if too many selections are being made etc, the label/warning text shoudl be changed
        ListDataProvider dataProvider = new ListDataProvider();

        dataProvider.addItem(new MapDataItem().add("direction", "Management").add("value", getScoreRelToMax('scoreGovManagement')));
        dataProvider.addItem(new MapDataItem().add("direction", "Shareholder Rights").add("value", getScoreRelToMax('scoreGovShareholders')));
        dataProvider.addItem(new MapDataItem().add("direction", "CSR Policy").add("value", getScoreRelToMax('scoreGovCSR')));
        dataProvider.addItem(new MapDataItem().add("direction", "Workforce").add("value", getScoreRelToMax('scoreSocialWorkforce')));
        dataProvider.addItem(new MapDataItem().add("direction", "Community").add("value", getScoreRelToMax('scoreSocialCommunity')));
        dataProvider.addItem(new MapDataItem().add("direction", "Responsible Products").add("value", getScoreRelToMax('scoreSocialProdResp')));
        dataProvider.addItem(new MapDataItem().add("direction", "Income").add("value", getScoreRelToMax('yield'))); // TODO these three need to be determined
        dataProvider.addItem(new MapDataItem().add("direction", "Cost").add("value", 100-(getScoreRelToMax('ter')*10)));
        dataProvider.addItem(new MapDataItem().add("direction", "Historic Perf").add("value", getScoreRelToMax('perf5Y')));
        dataProvider.addItem(new MapDataItem().add("direction", "Resource Use").add("value", getScoreRelToMax('scoreEnvResUse')));
        dataProvider.addItem(new MapDataItem().add("direction", "Emissions").add("value", getScoreRelToMax('scoreEnvEmissions')));
        dataProvider.addItem(new MapDataItem().add("direction", "Innovation").add("value", getScoreRelToMax('scoreEnvInnovation')));


        def guide1 = new Guide()
        guide1.angle = 45
        guide1.toAngle = 135
        guide1.toValue = 14
        guide1.fillColor = Color.ALICEBLUE
        guide1.fillAlpha = 0.3
        guide1.value = 0
        guide1.tickLength = 0
        guide1.lineAlpha = 0

        polarChart.addGuides(guide1)

        polarChart.setDataProvider(dataProvider);

    }

    public void goalChanger(Component source) {

        Button goalButton = (Button)source;
        //goalButton.setIcon(ICON_MINOR_INTEREST);

        Integer currentValue = getItem().getValue(goalButton.getId());

        Portfolio item = getItem();

        if (currentValue == null)
            item.setValue(goalButton.getId(), 1);
        else if (currentValue == 2)
            item.setValue(goalButton.getId(), 0);
        else
            item.setValue(goalButton.getId(), ++currentValue);

        setItem(portfolioService.determineHoldings(item));

        repaintIcons()
        redrawChart()

        /*
        1. Use a matrix to lookup the portfolio attribute associated with the button (or name them the same)
        2. Look at the value associated with that attributed, either increment it by 1 or zero it out depending on current value
        3. Call repaint function which will set all the page icons depending on the portfolio value-09
        4. Call some kind of portfolio mechanism that will adjust the holdings
        5. Use the combined values of the generated holdings to redraw the radar chart
         */
    }

    void repaintIcons() {

        def port = portfolioService.determineHoldings(getItem())

        getComponents().findAll{it.class.equals(com.haulmont.cuba.web.gui.components.WebButton.class)}.each {
            def button = it as WebButton

            if (port.hasProperty(button.getId())) {
                switch (port.getValue(button.getId()) as Integer) {
                    case 0:
                        button.setIcon(ICON_NO_INTEREST)
                        break;
                    case 1:
                        button.setIcon(ICON_MINOR_INTEREST)
                        break;
                    case 2:
                        button.setIcon(ICON_MAJOR_INTEREST)
                        break;
                }
            }
        }
        // 1. Find all buttons on page
        // 2. Look for a property of the current portfolio with a matching name
        // 3. If one is found, repaint the icon as appropriate
    }
}
package com.company.portfoliopoc.web.screens.sector;

import com.haulmont.cuba.gui.screen.*;
import com.company.portfoliopoc.entity.Sector;

@UiController("portfoliopoc_Sector.browse")
@UiDescriptor("sector-browse.xml")
@LookupComponent("table")
@LoadDataBeforeShow
public class SectorBrowse extends MasterDetailScreen<Sector> {
}
package com.company.portfoliopoc.web.screens.security;

import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.company.portfoliopoc.entity.Security;
import com.haulmont.cuba.web.gui.components.table.GroupTableDataContainer;

import javax.inject.Inject;

@UiController("portfoliopoc_Security.browse")
@UiDescriptor("security-browse.xml")
@LookupComponent("securitiesTable")
@LoadDataBeforeShow
public class SecurityBrowse extends StandardLookup<com.company.portfoliopoc.entity.Security> {
    @Inject
    private Notifications notifications;

    public void commtiClick() {
        getScreenData().getDataContext().commit();
    }
}
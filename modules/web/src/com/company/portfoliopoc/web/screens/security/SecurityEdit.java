package com.company.portfoliopoc.web.screens.security;

import com.haulmont.cuba.gui.screen.*;
import com.company.portfoliopoc.entity.Security;

@UiController("portfoliopoc_Security.edit")
@UiDescriptor("security-edit.xml")
@EditedEntityContainer("securityDc")
@LoadDataBeforeShow
public class SecurityEdit extends StandardEditor<Security> {
}
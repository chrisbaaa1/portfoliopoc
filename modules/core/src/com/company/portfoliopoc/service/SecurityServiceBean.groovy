package com.company.portfoliopoc.service

import com.company.portfoliopoc.entity.Security
import com.haulmont.cuba.core.global.DataManager
import com.haulmont.cuba.core.global.LoadContext;
import org.springframework.stereotype.Service

import javax.inject.Inject;

@Service(SecurityService.NAME)
public class SecurityServiceBean implements SecurityService {

    @Inject
    protected DataManager dataManager

    @Override
    public List<Security> getSecuritiesByCategory(int category) {
        return dataManager.loadList(LoadContext.create(Security.class).setQuery(
                LoadContext.createQuery('select e from portfoliopoc_Security e where e.category = :catId')
                        .setParameter('catId', category)).setView('security-view'))
    }

    @Override
    public List<Security> getSecurities() {
        return dataManager.load(Security.class).view('security-view').list()
    }

    @Override
    public String getSecPropertyForPort(String portfolioProp) {
        switch (portfolioProp) {
            case 'importanceWorkforce':
                 return 'scoreSocialWorkforce'
            case 'importanceRights':
                return 'scoreSocialRights'
            case 'importanceCommunity':
                return 'scoreSocialCommunity'
            case 'importanceMangement':
                return 'scoreGovManagement'
            case 'importanceShareholder':
                return 'scoreGovShareholders'
            case 'importanceCSR':
                return 'scoreGovCSR'
            case 'importanceEmissions':
                return 'scoreEnvEmissions'
            case 'importanceInnovation':
                return 'scoreEnvInnovation'
            case 'importanceResourceUse':
                return 'scoreEnvResUse'
            case 'importanceYield':
                return 'yield'
            case 'importanceCost':
                return 'ter'
            case 'importancePerf':
                return 'perf5Y'
        }
    }
}
package com.company.portfoliopoc.service

import com.company.portfoliopoc.entity.Portfolio
import com.company.portfoliopoc.entity.ProposedHolding
import com.company.portfoliopoc.entity.Security
import com.haulmont.cuba.core.global.Metadata
import org.apache.commons.math3.util.Pair
import org.springframework.stereotype.Service

import javax.inject.Inject;

@Service(PortfolioService.NAME)
public class PortfolioServiceBean implements PortfolioService {

    @Inject
    protected SecurityService securityService

    @Inject
    protected Metadata metadata

    @Override
    public Portfolio determineHoldings(Portfolio input) {
        // 1. Look up matrix of suggested portfolio split (percentage to be assigned to each sector?)
        //def portfolioBaseline = [1: 20, 2: 10, 3: 10, 4: 25, 5: 25, 6: 5, 7: 5]
        // This is our 'managed balanced' base portfolio

        if (!input.model)
            throw new Exception("Investment model must be specified.")

        def totalInvest = input.proposedValue ? input.proposedValue : new BigDecimal(100000)
        // If this hasn't been set, assume a portfolio value of £100k

        input.proposedHoldings = []

        input.model.lines.each {
            position ->

                def matchingSecurities = securityService.getSecuritiesByCategory(position.category)

                // Now we need to find the security from this group that most closely matches the individuals criteria
                // Then we assign it a value which is a percentage of the total portfolio value

                // Look for anything with a preference of '2' - the best scoring company in each of these must be in the outcome
                // Totalise these and this is our initial score for this security
                // Then, work out which combination of '1' soores will lead to the highest toal outcome score, and set the security to that
                def scores = new HashMap<Security, Integer>()
                matchingSecurities.each {
                    scores.put(it, 0)
                }

                // TODO new to 'screen out' all companies which violate the users desires around negative screening

                // If this category is most important (i.e. a 2) add this score to the total for this security
                input.getProperties().findAll { it.key.toString().contains('importance') && it.value.equals(2) }.each {
                    def pref = it.value as Integer

                    if (pref && pref != 0) {
                        def highestSec = getHighestScoring(scores.keySet().toList(), it.key as String)
                        scores[highestSec.first] = highestSec.second

                    }
                }

                // Then go through all areas of interest and add their scores. Because we've already added a score for the security with the highest in this area, we're effective;ly
                // double counting it and giving it more importance to the final outcome
                input.getProperties().findAll { it.key.toString().contains('importance') && (it.value.equals(1) || it.value.equals(2))}.each {
                    def pref = it.value as Integer

                    if (pref && pref != 0) {
                        scores.keySet().each {
                            sec ->
                                scores[sec] = scores[sec] + getScore(sec, it.key as String)
                        }
                    }
                }

                // Now interrogate the scores to find the highest

                def holding = metadata.create(ProposedHolding.class)
                holding.security = scores.max { it.value }.key
                holding.totalScore = scores.max { it.value }.value
                holding.value = totalInvest / 100 * position.percentage

                input.proposedHoldings.add(holding)

        }

        // 2. For each portfolio position, determine the pool of potential investments that can go into that position
        // 3. Exclude any from the pool which need to be actively 'screened' out (to be done later)
        // 4. Determine which of the investments for that position is the "Most" appropriate based on the criteria chosen
        return input
    }

    Tuple2<Security, Integer> getHighestScoring(List<Security> possibleSecurities, String property) {
        // If importance is 2, set it to the highest in the list
        switch (property) {
            case 'importanceWorkforce':
                def sec = (possibleSecurities.max { it.scoreSocialWorkforce })
                return new Tuple2(sec, sec.scoreSocialWorkforce)
            case 'importanceRights':
                def sec = (possibleSecurities.max { it.scoreSocialRights })
                return new Tuple2(sec, sec.scoreSocialRights)
            case 'importanceCommunity':
                def sec = (possibleSecurities.max { it.scoreSocialCommunity })
                return new Tuple2(sec, sec.scoreSocialCommunity)
            case 'importanceMangement':
                def sec = (possibleSecurities.max { it.scoreGovManagement })
                return new Tuple2(sec, sec.scoreGovManagement)
            case 'importanceShareholder':
                def sec = (possibleSecurities.max { it.scoreGovShareholders })
                return new Tuple2(sec, sec.scoreGovShareholders)
            case 'importanceCSR':
                def sec = (possibleSecurities.max { it.scoreGovCSR })
                return new Tuple2(sec, sec.scoreGovCSR)
            case 'importanceEmissions':
                def sec = (possibleSecurities.max { it.scoreEnvEmissions })
                return new Tuple2(sec, sec.scoreEnvEmissions)
            case 'importanceInnovation':
                def sec = (possibleSecurities.max { it.scoreEnvInnovation })
                return new Tuple2(sec, sec.scoreEnvInnovation)
            case 'importanceResourceUse':
                def sec = (possibleSecurities.max { it.scoreEnvResUse })
                return new Tuple2(sec, sec.scoreEnvResUse)
            case 'importanceYield':
                def sec = (possibleSecurities.max { it.yield })
                return new Tuple2(sec, sec.yield.toInteger())
            case 'importanceCost':
                def sec = (possibleSecurities.min { it.ter })
                return new Tuple2(sec, sec.ter ? sec.ter.toInteger() : 0)
            case 'importancePerf':
                def sec = (possibleSecurities.max { it.perf5Y })
                return new Tuple2(sec, sec.perf5Y ? sec.perf5Y.toInteger() : 0)
        }
    }

    Integer getScore(Security sec, String property) {
        switch (property) {
            case 'importanceWorkforce':
                return sec.scoreSocialWorkforce
                break;
            case 'importanceRights':
                return sec.scoreSocialRights
            case 'importanceCommunity':
                return sec.scoreSocialCommunity
            case 'importanceMangement':
                return sec.scoreGovManagement
            case 'importanceShareholder':
                return sec.scoreGovShareholders
            case 'importanceCSR':
                return sec.scoreGovCSR
            case 'importanceEmissions':
                return sec.scoreEnvEmissions
            case 'importanceInnovation':
                return sec.scoreEnvInnovation
            case 'importanceResourceUse':
                return sec.scoreEnvResUse
            case 'importanceYield':
                return sec.yield
            case 'importanceCost':
                return sec.ter ? 10 - sec.ter.toInteger() : 0 // We'll take a theoretical max (10) and subtract the TER from it to get an inverse score to help us
            case 'importancePerf':
                return sec.perf5Y ? sec.perf5Y.toInteger() : 0
        }
    }

    @Override
    String getPortPropertyFromSec(String portfolioProp) {
        switch (portfolioProp) {
            case 'scoreSocialWorkforce':
                return 'importanceWorkforce'
            case 'scoreSocialRights':
                return 'importanceRights'
            case 'scoreSocialCommunity':
                return 'importanceCommunity'
            case 'scoreGovManagement':
                return 'importanceMangement'
            case 'scoreGovShareholders':
                return 'importanceShareholder'
            case 'scoreGovCSR':
                return 'importanceCSR'
            case 'scoreEnvEmissions':
                return 'importanceEmissions'
            case 'scoreEnvInnovation':
                return 'importanceInnovation'
            case 'scoreEnvResUse':
                return 'importanceResourceUse'
            case 'yield':
                return 'importanceYield'
            case 'ter':
                return 'importanceCost'
            case 'perf5Y':
                return 'importancePerf'

        }

    }
}
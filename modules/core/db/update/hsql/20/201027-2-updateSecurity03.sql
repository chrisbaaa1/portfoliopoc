alter table PORTFOLIOPOC_SECURITY add constraint FK_PORTFOLIOPOC_SECURITY_ON_SECTOR foreign key (SECTOR_ID) references PORTFOLIOPOC_SECTOR(ID);
create index IDX_PORTFOLIOPOC_SECURITY_ON_SECTOR on PORTFOLIOPOC_SECURITY (SECTOR_ID);

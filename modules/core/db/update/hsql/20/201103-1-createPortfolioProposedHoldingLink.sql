create table PORTFOLIOPOC_PORTFOLIO_PROPOSED_HOLDING_LINK (
    PORTFOLIO_ID varchar(36) not null,
    PROPOSED_HOLDING_ID varchar(36) not null,
    primary key (PORTFOLIO_ID, PROPOSED_HOLDING_ID)
);

create table PORTFOLIOPOC_PORTFOLIO_SECURITY_LINK (
    PORTFOLIO_ID varchar(36) not null,
    SECURITY_ID varchar(36) not null,
    primary key (PORTFOLIO_ID, SECURITY_ID)
);

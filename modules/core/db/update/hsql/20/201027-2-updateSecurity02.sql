alter table PORTFOLIOPOC_SECURITY alter column SECTOR rename to SECTOR__U29771 ^
alter table PORTFOLIOPOC_SECURITY add column PERF3M varchar(255) ;
alter table PORTFOLIOPOC_SECURITY add column PERF1M decimal(19, 2) ;
alter table PORTFOLIOPOC_SECURITY add column PERF1W decimal(19, 2) ;
alter table PORTFOLIOPOC_SECURITY add column SECTOR_ID varchar(36) ;
alter table PORTFOLIOPOC_SECURITY add column PERF5Y decimal(19, 2) ;
alter table PORTFOLIOPOC_SECURITY add column PERF3Y decimal(19, 2) ;
alter table PORTFOLIOPOC_SECURITY add column PERF6M decimal(19, 2) ;
alter table PORTFOLIOPOC_SECURITY add column PERF2Y decimal(19, 2) ;
alter table PORTFOLIOPOC_SECURITY add column PERF1Y decimal(19, 2) ;

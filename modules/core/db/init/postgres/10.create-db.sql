-- begin PORTFOLIOPOC_PROPOSED_HOLDING
create table PORTFOLIOPOC_PROPOSED_HOLDING (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SECURITY_ID uuid,
    TOTAL_SCORE integer,
    VALUE_ decimal(19, 2),
    --
    primary key (ID)
)^
-- end PORTFOLIOPOC_PROPOSED_HOLDING
-- begin PORTFOLIOPOC_SECTOR
create table PORTFOLIOPOC_SECTOR (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    --
    primary key (ID)
)^
-- end PORTFOLIOPOC_SECTOR
-- begin PORTFOLIOPOC_MODEL
create table PORTFOLIOPOC_MODEL (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    --
    primary key (ID)
)^
-- end PORTFOLIOPOC_MODEL
-- begin PORTFOLIOPOC_MODEL_LINE
create table PORTFOLIOPOC_MODEL_LINE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CATEGORY integer,
    PERCENTAGE integer,
    MODEL_ID uuid,
    --
    primary key (ID)
)^
-- end PORTFOLIOPOC_MODEL_LINE
-- begin PORTFOLIOPOC_PORTFOLIO
create table PORTFOLIOPOC_PORTFOLIO (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    MODEL_ID uuid,
    PROPOSED_VALUE decimal(19, 2),
    IMPORTANCE_WORKFORCE integer,
    IMPORTANCE_RIGHTS integer,
    IMPORTANCE_COMMUNITY integer,
    IMPORTANCE_MANGEMENT integer,
    IMPORTANCE_SHAREHOLDER integer,
    IMPORTANCE_CSR integer,
    IMPORTANCE_EMISSIONS integer,
    IMPORTANCE_INNOVATION integer,
    IMPORTANCE_RESOURCE_USE integer,
    SCREEN_ANIMAL_TESTING boolean,
    SCREEN_ETHICS_CONTROVERSY boolean,
    SCREEN_LOBBYING boolean,
    SCREEN_ENERGY_INTENSIVE boolean,
    SCREEN_FOSSIL_FUELS boolean,
    SCREEN_GEENHOUSE boolean,
    SCREEN_HAZARDOUS_WASTE boolean,
    SCREEN_TOBACCO boolean,
    SCREEN_ALCOHOL boolean,
    SCREEN_GAMBLING boolean,
    SCREEN_WEAPONS boolean,
    DESIRED_YIELD integer,
    DESIRED_LOW_COST integer,
    DESIRED_HISTORIC_PERF integer,
    --
    primary key (ID)
)^
-- end PORTFOLIOPOC_PORTFOLIO
-- begin PORTFOLIOPOC_SECURITY
create table PORTFOLIOPOC_SECURITY (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    TICKER varchar(5),
    CATEGORY integer,
    TYPE_ integer,
    SECTOR_ID uuid,
    CAP decimal(19, 2),
    YIELD decimal(19, 2),
    TER decimal(19, 2),
    SCORE_ESG integer,
    SCORE_CONTROVERSY integer,
    SCORE_COMBED integer,
    SCORE_ENV integer,
    SCORE_ENV_RES_USE integer,
    SCORE_ENV_EMISSIONS integer,
    SCORE_ENV_INNOVATION integer,
    SCORE_SOCIAL integer,
    SCORE_SOCIAL_WORKFORCE integer,
    SCORE_SOCIAL_RIGHTS integer,
    SCORE_SOCIAL_COMMUNITY integer,
    SCORE_SOCIAL_PROD_RESP integer,
    SCORE_GOVERNANCE integer,
    SCORE_GOV_MANAGEMENT integer,
    SCORE_GOV_SHAREHOLDERS integer,
    SCORE_GOV_CSR integer,
    MARKET_DRUGS_TAB_ALC boolean,
    MARKET_ANIMAL_TESTING boolean,
    PERF1W decimal(19, 2),
    PERF1M decimal(19, 2),
    PERF3M varchar(255),
    PERF6M decimal(19, 2),
    PERF1Y decimal(19, 2),
    PERF2Y decimal(19, 2),
    PERF3Y decimal(19, 2),
    PERF5Y decimal(19, 2),
    --
    primary key (ID)
)^
-- end PORTFOLIOPOC_SECURITY
-- begin PORTFOLIOPOC_PORTFOLIO_PROPOSED_HOLDING_LINK
create table PORTFOLIOPOC_PORTFOLIO_PROPOSED_HOLDING_LINK (
    PORTFOLIO_ID uuid,
    PROPOSED_HOLDING_ID uuid,
    primary key (PORTFOLIO_ID, PROPOSED_HOLDING_ID)
)^
-- end PORTFOLIOPOC_PORTFOLIO_PROPOSED_HOLDING_LINK

package com.company.portfoliopoc.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "PORTFOLIOPOC_PROPOSED_HOLDING")
@Entity(name = "portfoliopoc_ProposedHolding")
public class ProposedHolding extends StandardEntity {
    private static final long serialVersionUID = -4851137605229170370L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SECURITY_ID")
    protected Security security;

    @Column(name = "TOTAL_SCORE")
    protected Integer totalScore;

    @Column(name = "VALUE_")
    protected BigDecimal value;

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }
}
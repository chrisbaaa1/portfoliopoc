package com.company.portfoliopoc.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@NamePattern("%s|name")
@Table(name = "PORTFOLIOPOC_PORTFOLIO")
@Entity(name = "portfoliopoc_Portfolio")
public class Portfolio extends StandardEntity {
    private static final long serialVersionUID = -6565401971360914277L;

    @Column(name = "NAME")
    protected String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODEL_ID")
    protected Model model;

    @Column(name = "PROPOSED_VALUE")
    protected BigDecimal proposedValue;

    @Column(name = "IMPORTANCE_WORKFORCE")
    protected Integer importanceWorkforce;

    @Column(name = "IMPORTANCE_RIGHTS")
    protected Integer importanceRights;

    @Column(name = "IMPORTANCE_COMMUNITY")
    protected Integer importanceCommunity;

    @Column(name = "IMPORTANCE_MANGEMENT")
    protected Integer importanceMangement;

    @Column(name = "IMPORTANCE_SHAREHOLDER")
    protected Integer importanceShareholder;

    @Column(name = "IMPORTANCE_CSR")
    protected Integer importanceCSR;

    @Column(name = "IMPORTANCE_EMISSIONS")
    protected Integer importanceEmissions;

    @Column(name = "IMPORTANCE_INNOVATION")
    protected Integer importanceInnovation;

    @Column(name = "IMPORTANCE_RESOURCE_USE")
    protected Integer importanceResourceUse;

    @Column(name = "SCREEN_ANIMAL_TESTING")
    protected Boolean screenAnimalTesting;

    @Column(name = "SCREEN_ETHICS_CONTROVERSY")
    protected Boolean screenEthicsControversy;

    @Column(name = "SCREEN_LOBBYING")
    protected Boolean screenLobbying;

    @Column(name = "SCREEN_ENERGY_INTENSIVE")
    protected Boolean screenEnergyIntensive;

    @Column(name = "SCREEN_FOSSIL_FUELS")
    protected Boolean screenFossilFuels;

    @Column(name = "SCREEN_GEENHOUSE")
    protected Boolean screenGeenhouse;

    @Column(name = "SCREEN_HAZARDOUS_WASTE")
    protected Boolean screenHazardousWaste;

    @Column(name = "SCREEN_TOBACCO")
    protected Boolean screenTobacco;

    @Column(name = "SCREEN_ALCOHOL")
    protected Boolean screenAlcohol;

    @Column(name = "SCREEN_GAMBLING")
    protected Boolean screenGambling;

    @Column(name = "SCREEN_WEAPONS")
    protected Boolean screenWeapons;

    @Column(name = "DESIRED_YIELD")
    protected Integer importanceYield;

    @Column(name = "DESIRED_LOW_COST")
    protected Integer importanceCost;

    @Column(name = "DESIRED_HISTORIC_PERF")
    protected Integer importancePerf;

    @JoinTable(name = "PORTFOLIOPOC_PORTFOLIO_PROPOSED_HOLDING_LINK",
            joinColumns = @JoinColumn(name = "PORTFOLIO_ID"),
            inverseJoinColumns = @JoinColumn(name = "PROPOSED_HOLDING_ID"))
    @ManyToMany
    protected List<ProposedHolding> proposedHoldings;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public void setProposedHoldings(List<ProposedHolding> proposedHoldings) {
        this.proposedHoldings = proposedHoldings;
    }

    public List<ProposedHolding> getProposedHoldings() {
        return proposedHoldings;
    }

    public BigDecimal getProposedValue() {
        return proposedValue;
    }

    public void setProposedValue(BigDecimal proposedValue) {
        this.proposedValue = proposedValue;
    }

    public void setImportanceEmissions(Integer importanceEmissions) {
        this.importanceEmissions = importanceEmissions;
    }

    public Integer getImportanceEmissions() {
        return importanceEmissions;
    }

    public Integer getImportancePerf() {
        return importancePerf;
    }

    public void setImportancePerf(Integer importancePerf) {
        this.importancePerf = importancePerf;
    }

    public Integer getImportanceCost() {
        return importanceCost;
    }

    public void setImportanceCost(Integer importanceCost) {
        this.importanceCost = importanceCost;
    }

    public Integer getImportanceYield() {
        return importanceYield;
    }

    public void setImportanceYield(Integer importanceYield) {
        this.importanceYield = importanceYield;
    }

    public Boolean getScreenWeapons() {
        return screenWeapons;
    }

    public void setScreenWeapons(Boolean screenWeapons) {
        this.screenWeapons = screenWeapons;
    }

    public Boolean getScreenGambling() {
        return screenGambling;
    }

    public void setScreenGambling(Boolean screenGambling) {
        this.screenGambling = screenGambling;
    }

    public Boolean getScreenAlcohol() {
        return screenAlcohol;
    }

    public void setScreenAlcohol(Boolean screenAlcohol) {
        this.screenAlcohol = screenAlcohol;
    }

    public Boolean getScreenTobacco() {
        return screenTobacco;
    }

    public void setScreenTobacco(Boolean screenTobacco) {
        this.screenTobacco = screenTobacco;
    }

    public Boolean getScreenHazardousWaste() {
        return screenHazardousWaste;
    }

    public void setScreenHazardousWaste(Boolean screenHazardousWaste) {
        this.screenHazardousWaste = screenHazardousWaste;
    }

    public Boolean getScreenGeenhouse() {
        return screenGeenhouse;
    }

    public void setScreenGeenhouse(Boolean screenGeenhouse) {
        this.screenGeenhouse = screenGeenhouse;
    }

    public Boolean getScreenFossilFuels() {
        return screenFossilFuels;
    }

    public void setScreenFossilFuels(Boolean screenFossilFuels) {
        this.screenFossilFuels = screenFossilFuels;
    }

    public Boolean getScreenEnergyIntensive() {
        return screenEnergyIntensive;
    }

    public void setScreenEnergyIntensive(Boolean screenEnergyIntensive) {
        this.screenEnergyIntensive = screenEnergyIntensive;
    }

    public Boolean getScreenLobbying() {
        return screenLobbying;
    }

    public void setScreenLobbying(Boolean screenLobbying) {
        this.screenLobbying = screenLobbying;
    }

    public Boolean getScreenEthicsControversy() {
        return screenEthicsControversy;
    }

    public void setScreenEthicsControversy(Boolean screenEthicsControversy) {
        this.screenEthicsControversy = screenEthicsControversy;
    }

    public Boolean getScreenAnimalTesting() {
        return screenAnimalTesting;
    }

    public void setScreenAnimalTesting(Boolean screenAnimalTesting) {
        this.screenAnimalTesting = screenAnimalTesting;
    }

    public Integer getImportanceResourceUse() {
        return importanceResourceUse;
    }

    public void setImportanceResourceUse(Integer importanceResourceUse) {
        this.importanceResourceUse = importanceResourceUse;
    }

    public Integer getImportanceInnovation() {
        return importanceInnovation;
    }

    public void setImportanceInnovation(Integer importanceInnovation) {
        this.importanceInnovation = importanceInnovation;
    }

    public Integer getImportanceCSR() {
        return importanceCSR;
    }

    public void setImportanceCSR(Integer importanceCSR) {
        this.importanceCSR = importanceCSR;
    }

    public Integer getImportanceShareholder() {
        return importanceShareholder;
    }

    public void setImportanceShareholder(Integer importanceShareholder) {
        this.importanceShareholder = importanceShareholder;
    }

    public Integer getImportanceMangement() {
        return importanceMangement;
    }

    public void setImportanceMangement(Integer importanceMangement) {
        this.importanceMangement = importanceMangement;
    }

    public Integer getImportanceCommunity() {
        return importanceCommunity;
    }

    public void setImportanceCommunity(Integer importanceCommunity) {
        this.importanceCommunity = importanceCommunity;
    }

    public Integer getImportanceRights() {
        return importanceRights;
    }

    public void setImportanceRights(Integer importanceRights) {
        this.importanceRights = importanceRights;
    }

    public Integer getImportanceWorkforce() {
        return importanceWorkforce;
    }

    public void setImportanceWorkforce(Integer importanceWorkforce) {
        this.importanceWorkforce = importanceWorkforce;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
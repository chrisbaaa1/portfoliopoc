package com.company.portfoliopoc.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@NamePattern("%s|name")
@Table(name = "PORTFOLIOPOC_MODEL")
@Entity(name = "portfoliopoc_Model")
public class Model extends StandardEntity {
    private static final long serialVersionUID = 3703876241296437027L;

    @Column(name = "NAME")
    protected String name;

    @OneToMany(mappedBy = "model")
    protected List<ModelLine> lines;

    public List<ModelLine> getLines() {
        return lines;
    }

    public void setLines(List<ModelLine> lines) {
        this.lines = lines;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
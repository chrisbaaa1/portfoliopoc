package com.company.portfoliopoc.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@NamePattern("%s|name")
@Table(name = "PORTFOLIOPOC_SECTOR")
@Entity(name = "portfoliopoc_Sector")
public class Sector extends StandardEntity {
    private static final long serialVersionUID = 4008880941868744100L;

    @Column(name = "NAME")
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
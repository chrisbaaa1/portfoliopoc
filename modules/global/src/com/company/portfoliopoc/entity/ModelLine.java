package com.company.portfoliopoc.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;

@Table(name = "PORTFOLIOPOC_MODEL_LINE")
@Entity(name = "portfoliopoc_ModelLine")
public class ModelLine extends StandardEntity {
    private static final long serialVersionUID = -792312040735707707L;

    @Column(name = "CATEGORY")
    protected Integer category;

    @Column(name = "PERCENTAGE")
    protected Integer percentage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODEL_ID")
    protected Model model;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }
}
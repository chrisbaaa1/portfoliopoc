package com.company.portfoliopoc.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@NamePattern("%s|name")
@Table(name = "PORTFOLIOPOC_SECURITY")
@Entity(name = "portfoliopoc_Security")
public class Security extends StandardEntity {
    private static final long serialVersionUID = 5250036733242427007L;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "TICKER", length = 5)
    protected String ticker;

    @Column(name = "CATEGORY")
    protected Integer category;

    @Column(name = "TYPE_")
    protected Integer type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SECTOR_ID")
    protected Sector sector;

    @Column(name = "CAP")
    protected BigDecimal cap;

    @Column(name = "YIELD")
    protected BigDecimal yield;

    @Column(name = "TER")
    protected BigDecimal ter;

    @Column(name = "SCORE_ESG")
    protected Integer scoreEsg;

    @Column(name = "SCORE_CONTROVERSY")
    protected Integer scoreControversy;

    @Column(name = "SCORE_COMBED")
    protected Integer scoreCombed;

    @Column(name = "SCORE_ENV")
    protected Integer scoreEnv;

    @Column(name = "SCORE_ENV_RES_USE")
    protected Integer scoreEnvResUse;

    @Column(name = "SCORE_ENV_EMISSIONS")
    protected Integer scoreEnvEmissions;

    @Column(name = "SCORE_ENV_INNOVATION")
    protected Integer scoreEnvInnovation;

    @Column(name = "SCORE_SOCIAL")
    protected Integer scoreSocial;

    @Column(name = "SCORE_SOCIAL_WORKFORCE")
    protected Integer scoreSocialWorkforce;

    @Column(name = "SCORE_SOCIAL_RIGHTS")
    protected Integer scoreSocialRights;

    @Column(name = "SCORE_SOCIAL_COMMUNITY")
    protected Integer scoreSocialCommunity;

    @Column(name = "SCORE_SOCIAL_PROD_RESP")
    protected Integer scoreSocialProdResp;

    @Column(name = "SCORE_GOVERNANCE")
    protected Integer scoreGovernance;

    @Column(name = "SCORE_GOV_MANAGEMENT")
    protected Integer scoreGovManagement;

    @Column(name = "SCORE_GOV_SHAREHOLDERS")
    protected Integer scoreGovShareholders;

    @Column(name = "SCORE_GOV_CSR")
    protected Integer scoreGovCSR;

    @Column(name = "MARKET_DRUGS_TAB_ALC")
    protected Boolean marketDrugsTabAlc;

    @Column(name = "MARKET_ANIMAL_TESTING")
    protected Boolean marketAnimalTesting;

    @Column(name = "PERF1W")
    protected BigDecimal perf1W;

    @Column(name = "PERF1M")
    protected BigDecimal perf1M;

    @Column(name = "PERF3M")
    protected String perf3M;

    @Column(name = "PERF6M")
    protected BigDecimal perf6M;

    @Column(name = "PERF1Y")
    protected BigDecimal perf1Y;

    @Column(name = "PERF2Y")
    protected BigDecimal perf2Y;

    @Column(name = "PERF3Y")
    protected BigDecimal perf3Y;

    @Column(name = "PERF5Y")
    protected BigDecimal perf5Y;

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public BigDecimal getPerf5Y() {
        return perf5Y;
    }

    public void setPerf5Y(BigDecimal perf5Y) {
        this.perf5Y = perf5Y;
    }

    public BigDecimal getPerf3Y() {
        return perf3Y;
    }

    public void setPerf3Y(BigDecimal perf3Y) {
        this.perf3Y = perf3Y;
    }

    public BigDecimal getPerf2Y() {
        return perf2Y;
    }

    public void setPerf2Y(BigDecimal perf2Y) {
        this.perf2Y = perf2Y;
    }

    public BigDecimal getPerf1Y() {
        return perf1Y;
    }

    public void setPerf1Y(BigDecimal perf1Y) {
        this.perf1Y = perf1Y;
    }

    public BigDecimal getPerf6M() {
        return perf6M;
    }

    public void setPerf6M(BigDecimal perf6M) {
        this.perf6M = perf6M;
    }

    public String getPerf3M() {
        return perf3M;
    }

    public void setPerf3M(String perf3M) {
        this.perf3M = perf3M;
    }

    public BigDecimal getPerf1M() {
        return perf1M;
    }

    public void setPerf1M(BigDecimal perf1M) {
        this.perf1M = perf1M;
    }

    public BigDecimal getPerf1W() {
        return perf1W;
    }

    public void setPerf1W(BigDecimal perf1W) {
        this.perf1W = perf1W;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public void setMarketAnimalTesting(Boolean marketAnimalTesting) {
        this.marketAnimalTesting = marketAnimalTesting;
    }

    public Boolean getMarketAnimalTesting() {
        return marketAnimalTesting;
    }

    public void setScoreSocialWorkforce(Integer scoreSocialWorkforce) {
        this.scoreSocialWorkforce = scoreSocialWorkforce;
    }

    public Integer getScoreSocialWorkforce() {
        return scoreSocialWorkforce;
    }

    public void setScoreSocialCommunity(Integer scoreSocialCommunity) {
        this.scoreSocialCommunity = scoreSocialCommunity;
    }

    public Integer getScoreSocialCommunity() {
        return scoreSocialCommunity;
    }

    public Boolean getMarketDrugsTabAlc() {
        return marketDrugsTabAlc;
    }

    public void setMarketDrugsTabAlc(Boolean marketDrugsTabAlc) {
        this.marketDrugsTabAlc = marketDrugsTabAlc;
    }

    public Integer getScoreGovCSR() {
        return scoreGovCSR;
    }

    public void setScoreGovCSR(Integer scoreGovCSR) {
        this.scoreGovCSR = scoreGovCSR;
    }

    public Integer getScoreGovShareholders() {
        return scoreGovShareholders;
    }

    public void setScoreGovShareholders(Integer scoreGovShareholders) {
        this.scoreGovShareholders = scoreGovShareholders;
    }

    public Integer getScoreGovManagement() {
        return scoreGovManagement;
    }

    public void setScoreGovManagement(Integer scoreGovManagement) {
        this.scoreGovManagement = scoreGovManagement;
    }

    public Integer getScoreGovernance() {
        return scoreGovernance;
    }

    public void setScoreGovernance(Integer scoreGovernance) {
        this.scoreGovernance = scoreGovernance;
    }

    public Integer getScoreSocialProdResp() {
        return scoreSocialProdResp;
    }

    public void setScoreSocialProdResp(Integer scoreSocialProdResp) {
        this.scoreSocialProdResp = scoreSocialProdResp;
    }

    public Integer getScoreSocialRights() {
        return scoreSocialRights;
    }

    public void setScoreSocialRights(Integer scoreSocialRights) {
        this.scoreSocialRights = scoreSocialRights;
    }

    public Integer getScoreSocial() {
        return scoreSocial;
    }

    public void setScoreSocial(Integer scoreSocial) {
        this.scoreSocial = scoreSocial;
    }

    public Integer getScoreEnvInnovation() {
        return scoreEnvInnovation;
    }

    public void setScoreEnvInnovation(Integer scoreEnvInnovation) {
        this.scoreEnvInnovation = scoreEnvInnovation;
    }

    public Integer getScoreEnvEmissions() {
        return scoreEnvEmissions;
    }

    public void setScoreEnvEmissions(Integer scoreEnvEmissions) {
        this.scoreEnvEmissions = scoreEnvEmissions;
    }

    public Integer getScoreEnvResUse() {
        return scoreEnvResUse;
    }

    public void setScoreEnvResUse(Integer scoreEnvResUse) {
        this.scoreEnvResUse = scoreEnvResUse;
    }

    public Integer getScoreEnv() {
        return scoreEnv;
    }

    public void setScoreEnv(Integer scoreEnv) {
        this.scoreEnv = scoreEnv;
    }

    public Integer getScoreCombed() {
        return scoreCombed;
    }

    public void setScoreCombed(Integer scoreCombed) {
        this.scoreCombed = scoreCombed;
    }

    public Integer getScoreControversy() {
        return scoreControversy;
    }

    public void setScoreControversy(Integer scoreControversy) {
        this.scoreControversy = scoreControversy;
    }

    public Integer getScoreEsg() {
        return scoreEsg;
    }

    public void setScoreEsg(Integer scoreEsg) {
        this.scoreEsg = scoreEsg;
    }

    public BigDecimal getTer() {
        return ter;
    }

    public void setTer(BigDecimal ter) {
        this.ter = ter;
    }

    public BigDecimal getYield() {
        return yield;
    }

    public void setYield(BigDecimal yield) {
        this.yield = yield;
    }

    public BigDecimal getCap() {
        return cap;
    }

    public void setCap(BigDecimal cap) {
        this.cap = cap;
    }

    public SecurityType getType() {
        return type == null ? null : SecurityType.fromId(type);
    }

    public void setType(SecurityType type) {
        this.type = type == null ? null : type.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
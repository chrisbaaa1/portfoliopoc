package com.company.portfoliopoc.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum SecurityType implements EnumClass<Integer> {

    EQUITY(10);

    private Integer id;

    SecurityType(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static SecurityType fromId(Integer id) {
        for (SecurityType at : SecurityType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
package com.company.portfoliopoc.service;

public interface PortfolioService {
    String NAME = "portfoliopoc_PortfolioService";

    String getPortPropertyFromSec(String portfolioProp);
    com.company.portfoliopoc.entity.Portfolio determineHoldings(com.company.portfoliopoc.entity.Portfolio input);
}
package com.company.portfoliopoc.service;

public interface SecurityService {
    String getSecPropertyForPort(String portfolioProp);
    java.util.List<com.company.portfoliopoc.entity.Security> getSecurities();
    
    java.util.List<com.company.portfoliopoc.entity.Security> getSecuritiesByCategory(int category);

    String NAME = "portfoliopoc_SecurityService";
}